 var URL = "http://localhost:9999/";
   //var URL = "http://45.79.209.142:9999/";

var app = angular.module('myApp', ['ngMaterial', 'ngMessages', 'ngRoute', 'lbServices', "checklist-model", 'datatables', 'ui-notification', 'ngAnimate', 'ngSanitize', 'ui.calendar', 'ui.bootstrap']);

app.config(function ($routeProvider, $locationProvider) {

  $routeProvider.when('/orderReports', {
    templateUrl: './orderReports.html',
    controller: 'orderReportsController'
  }).when('/userProfile', {
    templateUrl: './userProfile.html',
    controller: 'indexController'
  }).when('/filters', {
    templateUrl: './filters.html',
    controller: 'filtersController'
  }).when('/adminUsersList', {
    templateUrl: './adminUsersList.html',
    controller: 'adminUsersListController'
  }).when('/rejectedOrders', {
    templateUrl: './rejectedOrders.html',
    controller: 'rejectedOrdersController'
  }).when('/todayOrders', {
    templateUrl: './todayOrders.html',
    controller: 'todayOrdersController'
  }).when('/activeOrders', {
    templateUrl: './activeOrders.html',
    controller: 'activeOrdersController'
  }).when('/newOrders', {
    templateUrl: './newOrders.html',
    controller: 'newOrdersController'
  }).when('/orders', {
    templateUrl: './orders.html',
    controller: 'ordersController'
  }).when('/customer', {
    templateUrl: './customer.html',
    controller: 'customertController'
  }).when('/viewPackage', {
    templateUrl: './package.html',
    controller: 'viewPackageController'
  }).when('/viewMenu', {
    templateUrl: './viewMenu.html',
    controller: 'viewMenuController'
  }).when('/category', {
    templateUrl: './category.html',
    controller: 'categoryController'
  }).when('/product', {
    templateUrl: './product.html',
    controller: 'productController'
  }).when('/onlineShopping', {
    templateUrl: './onlineShopping.html',
    controller: 'onlineShoppingController'
  }).otherwise({redirectTo: '/'});

  /* if(window.history && window.history.pushState){
   //$locationProvider.html5Mode(true); will cause an error $location in HTML5 mode requires a  tag to be present! Unless you set baseUrl tag after head tag like so: <head> <base href="/">

   // to know more about setting base URL visit: https://docs.angularjs.org/error/$location/nobase
   // if you don't wish to set base URL then use this
   $locationProvider.html5Mode({
   enabled: true,
   requireBase: false
   });
   }*/

});

app.controller('indexController', function ($http, $scope, $window, $location, $rootScope, Excel, $timeout, Notification) {
  console.log("indexController");

  /* $http({
   "method": "GET",
   "url": URL + 'api/OrderRequests/getDateForCrm',
   "headers": {"Content-Type": "application/json", "Accept": "application/json"}
   }).success(function (response) {
   //console.log("today date" + response.data.Dates);
   $rootScope.todayDate = response.data.Dates;
   }).error(function (err) {
   Notification.error({message: 'Oopss.. error:114', delay: 1000});
   })*/

  $("#s1").on("click", function () {

    $(this).addClass("active").removeClass("active");
    /*$(this).removeClass('open').addClass('open');*/
    /*$(" .nav-second-level li a").removeClass("active");
     $(this).addClass("active");
     $(this).parent().parents("li").addClass("active");*/


  });

  $scope.newOrdersCount = function () {
    $http({
      method: 'GET',
      url: URL + 'api/Orders/count?where={"orderStatus":"NEW"}',
      headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
      // alert("err"+response.data.count);
      $rootScope.newOrdersCountNumber = response.data.count;
    }).error(function (error) {
      console.log("newOrdersCount err" + error);
    })
  };
  // $scope.newOrdersCount();

  $scope.todayOrdersCount = function () {
    $http({
      "method": "GET",
      "url": URL + 'api/OrderRequests/getDateForCrm',
      "headers": {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
      console.log("today date" + response.data.Dates);
      $http({
        method: 'GET',
        url: URL + 'api/Orders/count?where={"todaysOrdersDates":"' + response.data.Dates + '","orderStatus":"Accept"}',
        headers: {"Content-Type": "application/json", "Accept": "application/json"}
      }).success(function (response) {
        console.log("today count" + response.data.count);
        $rootScope.todayOrdersCountNumber = response.data.count;
      }).error(function (error) {
        console.log("failure");
      })
    }).error(function (err) {
      console.log("failure");
    });

  };
  //$scope.todayOrdersCount();
  $scope.loginerror = false;
  $rootScope.userDetails = JSON.parse($window.localStorage.getItem('userDetails'));
  $rootScope.userInfo = JSON.parse($window.localStorage.getItem('userInfo'));
  // console.log("xx" + JSON.stringify($rootScope.userInfo));
  $scope.user = {
    "email": "",
    "password": ""
  };
  $scope.login = function () {
    window.location.href = "./index.html";
    console.log("$scope.user" + JSON.stringify($scope.user));
   // var emal = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;

    /*if ($scope.user.email != '' && $scope.user.password != '') {
      if (emal.test($scope.user.email)) {
        $http({
          "method": "POST",
          "url": URL + "api/adminusers/login",
          "headers": {"Content-Type": "application/json", "Accept": "application/json"},
          "data": $scope.user
        }).success(function (response) {
          //console.log("response userDetails" + JSON.stringify(response));
          $window.localStorage.setItem('accessToken', response.id);
          $window.localStorage.setItem('userDetails', JSON.stringify(response));
          $http({
            "method": "GET",
            "url": URL + "api/adminusers?access_token=" + response.id,
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}

          }).success(function (res) {
            console.log("response userDetails" + JSON.stringify(res));
            $window.localStorage.setItem('userInfo', JSON.stringify(res));
          }).error(function (err) {
            $scope.loginerror = true;
            $scope.loginerror1 = err.error.message;
            //console.log("err" + $scope.loginerror1);
            Notification.error({message: 'Please Enter Correct User Name and Password', delay: 1000});
          });
          window.location.href = "./index.html";
        }).error(function (response) {
          $scope.loginerror = true;
          $scope.loginerror1 = response.error.message;
          //console.log("err" + $scope.loginerror1);
          Notification.error({message: 'Please Enter Correct User Name and Password', delay: 1000});

        });
      } else {
        Notification.error({message: 'Please Enter Valid Email', delay: 1000});
      }
    } else {
      Notification.error({message: 'Please Enter User Name and Password', delay: 1000});
    }*/
  };
  $scope.forgotPassword = function (email) {
    if (email != '') {
      $http({
        method: 'GET',
        url: URL + 'api/adminUsers/forgotPassword?email=' + email,
        headers: {"Content-Type": "application/json", "Accept": "application/json"}
      }).success(function (response) {
        document.getElementById("forgotEmail").value = '';
        $("#forgotPassword").modal("hide");
        Notification.success({message: 'Password Sent To Your Mobile'});
      }).error(function (error) {
        // alert()
        $scope.message = 'User dose not exist';
        $scope.messageSts = true;
        $timeout(function () {
          $scope.messageSts = false;
        }, 3000);
        // Notification.error({message: 'User  Not Exist', delay: 1000});
      })
    } else {
      Notification.error({message: 'Please Enter Email', delay: 1000});
    }
  }

  $scope.userNameChange = function (name, id) {
    $scope.accessToken = $window.localStorage.getItem('accessToken');
    $http({
      method: "PUT",
      url: URL + 'api/adminUsers/' + id, /*+ '?access_token=' + $scope.accessToken*/
      headers: {"Content-Type": "application/json", "Accept": "application/json"},
      data: {
        "name": name
      }
    }).success(function (response) {
      $window.localStorage.setItem('userInfo', JSON.stringify(response.data));
      $rootScope.userInfo = JSON.parse($window.localStorage.getItem('userInfo'));
      $("#changeNameField").modal('hide');
      $window.location.reload();
    }).error(function (response) {
    });

  }
  $scope.changeMobileNumber = function (userInfo) {
    $("#editPhone").modal("show");
    $scope.editMobileNumber = angular.copy(userInfo);
  }
  var phoneno = /^[0-9]{1}[0-9]{9}$/;
  $scope.editMobileSubmit = function (mobile, id) {
    if (phoneno.test($scope.editMobileNumber.mobile) || $scope.editMobileNumber.mobile < 10) {
      $scope.accessToken = $window.localStorage.getItem('accessToken');
      $http({
        method: "PUT",
        url: URL + 'api/adminUsers/' + id,
        headers: {"Content-Type": "application/json", "Accept": "application/json"},
        data: {
          "mobile": mobile
        }
      }).success(function (response) {
        $window.localStorage.setItem('userInfo', JSON.stringify(response.data));
        $rootScope.userInfo = JSON.parse($window.localStorage.getItem('userInfo'));
        $("#editPhone").modal('hide');
        $("#mobileNumberSuccess").modal('show');
        setTimeout(function () {
          $('#mobileNumberSuccess').modal('hide')
        }, 3000);
        //$window.location.reload();

      }).error(function (response) {
      });

    } else {
      $scope.errorMessage = true;
      $timeout(function () {
        $scope.errorMessage = false;
      }, 3000);
    }
  }


  $scope.editWorkNumSubmit = function (workNum, id) {
    if (phoneno.test(workNum) || workNum < 10) {
      $scope.accessToken = $window.localStorage.getItem('accessToken');
      $http({
        method: "PUT",
        url: URL + 'api/adminUsers/' + id,
        headers: {"Content-Type": "application/json", "Accept": "application/json"},
        data: {
          "workNum": workNum
        }
      }).success(function (response) {
        $window.localStorage.setItem('userInfo', JSON.stringify(response.data));
        $rootScope.userInfo = JSON.parse($window.localStorage.getItem('userInfo'));
        $window.location.reload();
        $("#editWorkNumField").modal('hide');
        $("#mobileNumberSuccess").modal('show');
        setTimeout(function () {
          $('#mobileNumberSuccess').modal('hide')
        }, 3000);
      }).error(function (response) {
      });
    } else {
      $scope.errorMessage = true;
      $timeout(function () {
        $scope.errorMessage = false;
      }, 3000);
    }

  };

  /*change password start*/
  $scope.changePasswordModal = function () {
    $('#changePassword').modal('show');
    /*$scope.user = {};*/
    document.getElementById("password").value = '';
    document.getElementById("pswd").value = '';
    document.getElementById("pswd1").value = '';
  }
  $scope.user = {};
  $scope.changePasswordSubmit = function () {

    $scope.tokenId = $window.localStorage.getItem("tokenId");
    //alert("tokenId" +$scope.tokenId);

    var currentpassword = document.getElementById("password").value;
    //alert("current password:::" +currentpassword);
    var password = document.getElementById("pswd").value;
    //alert(" password:::" +password);
    var cpasswor = document.getElementById("pswd1").value;
    //alert("confirm password:::" +cpasswor);
    $scope.passwordError1 = "";

    if (currentpassword != '' && currentpassword != undefined && currentpassword != null
      && password != '' && password != undefined && password != null
      && cpasswor != '' && cpasswor != undefined && cpasswor != null) {
      if (password.length >= 6) {
        if (currentpassword != password) {
          if (password == cpasswor) {
            $http({
              "method": "POST",
              "url": URL + 'api/adminUsers/login',
              "headers": {"Content-Type": "application/json", "Accept": "application/json"},
              "data": {
                "email": $rootScope.userDetails.userInfo.email,
                "password": currentpassword
              }
            }).success(function (response) {

              console.log("response" + JSON.stringify(response));
              $scope.tokenId = response.id;
              $scope.userId = response.userId;
              $window.localStorage.setItem('accessToken', $scope.tokenId);

              $http({
                method: "PUT",
                url: URL + "api/adminUsers/" + $scope.userId,
                headers: {'Accept': 'application/json'},
                data: {"password": password}
              }).success(function (response, data) {
                console.log("Password Response:" + JSON.stringify(response));
                //document.getElementById('myform').reset();
                $('#changePassword').modal('hide');
                $('#changePasswordSuccess').modal('show');
                setTimeout(function () {
                  $('#changePasswordSuccess').modal('hide')
                }, 2000);
                setTimeout(function () {
                  window.localStorage.removeItem('userDetails');
                  window.localStorage.removeItem('userInfo');
                  window.location.href = "login.html";
                }, 2000);

                //$scope.passwordError = "Password Updated Successfully";
                //console.log("Password Updated Successfully");

              }).error(function (response) {
                //console.log("Error:" + JSON.stringify(response));
                //$scope.passwordError = true;
                $scope.passwordError1 = "Something Went Wrong. Please Try Again Later";
                //console.log("Something Went Wrong. Please Try Again Later");

                $scope.errorMessagePSW = true;
                $timeout(function () {
                  $scope.errorMessagePSW = false;
                }, 3000);

              })
            }).error(function (data) {
              $scope.passwordError1 = "Please Enter Correct Password";
              console.log("Please Enter Correct Password");
              $scope.errorMessagePSW = true;
              $timeout(function () {
                $scope.errorMessagePSW = false;
              }, 3000);

            })
          } else {
            $scope.passwordError1 = "New password & confirm password does not match";
            //console.log("Password Confirmation Unsuccessful.");
            $scope.errorMessagePSW = true;
            $timeout(function () {
              $scope.errorMessagePSW = false;
            }, 3000);
          }
        } else {
          $scope.passwordError1 = "Current Password & Old Password Should Not Match";
          //console.log("Password Confirmation Unsuccessful.");
          $scope.errorMessagePSW = true;

        }
      }
      else {
        console.log("Enter Minimum 6 Characters");

      }
    } else {
      console.log("All Fields Are Mandatory.");

    }
  }


  /*change password end*/


});


app.controller('categoryController', function ($http, $scope, $window, $location, $rootScope, Excel, $timeout, Notification) {
  console.log("categoryController");

  $scope.viewCategories = function () {
    $http({
      "method": "GET",
      "url": URL + 'api/Categories',
      "headers": {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
      console.log("category " + JSON.stringify(response));
      $scope.categories = response;
    }).error(function (err) {
      console.log("failure");
    })


  };

  $scope.createCategory = function () {
    if ($scope.categoryData.categoryName) {
        if ($scope.categoryData.status) {
          $http({
            method: 'POST',
            url: URL + 'api/Categories',
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            "data": $scope.categoryData
          }).success(function (response) {
            $scope.viewCategories();
            document.getElementById("addmealTimeFormId").reset();
            $('#addmealTimePopId').modal('hide');
            Notification.success({message: 'Category Successfully added', delay: 5000});
          }).error(function (error) {
            Notification.error({message: error.error.message, delay: 2000});

          })
        } else {
          Notification.error({message: 'Please Select Status', delay: 1000});
        }

    } else {
      Notification.error({message: 'Please Enter categoryName', delay: 1000});
    }
  };

  //$scope.todayTop10select();
  $scope.editmealTimesavedata={};
  $scope.editmealTime=function(Category){
    /*$scope.editmealTimesavedata.categoryName=obj.categoryName;
    $scope.editmealTimesavedata.status=obj.status;
    $scope.editmealTimesavedata.id=obj.id;*/
    $scope.editmealTimesavedata = angular.copy(Category);
    $('#mealTimeEditpopId').modal('show');
  };

  $scope.editCategory = function (id) {
    $http({
      "method": "PUT",
      "url": URL + 'api/Categories',
      "headers": {"Content-Type": "application/json", "Accept": "application/json"},
      "data":$scope.editmealTimesavedata
    }).success(function (response) {
      $scope.viewCategories();
      $('#mealTimeEditpopId').modal('hide');
      Notification.success({message: 'Category Successfully Upadted', delay: 1000});
      console.log("837mealtimeedit " + JSON.stringify(response));
    }).error(function (err) {
      console.log("failure");
    })


  };
  $scope.exportToExcel = function (tableId) { // ex: '#my-table'
    var exportHref = Excel.tableToExcel(tableId, 'sheet name');
    $timeout(function () {
      location.href = exportHref;
    }, 100); // trigger download
  }
});


 app.controller('filtersController', function ($http, $scope, $window, $location, $rootScope, Excel, $timeout, Notification) {
   //console.log("filtersController");

   $http({
     "method": "GET",
     "url": URL + 'api/Categories',
     "headers": {"Content-Type": "application/json", "Accept": "application/json"}
   }).success(function (response) {
     console.log("category " + JSON.stringify(response));
     $scope.categoriesList = response;
   }).error(function (err) {
     console.log("failure");
   })



   $scope.getFilters = function () {

     $http({
       "method": "GET",
       "url": URL + 'api/Filters',
       "headers": {"Content-Type": "application/json", "Accept": "application/json"}
     }).success(function (response) {
       console.log("category " + JSON.stringify(response));
       $scope.filterList = response;
     }).error(function (err) {
       console.log("failure");
     })
   };

   $scope.createFilter = function () {

     $http({
       "method": "POST",
       "url": URL + 'api/Filters',
       "headers": {"Content-Type": "application/json", "Accept": "application/json"},
       "data" : $scope.filterData
     }).success(function (response) {
       console.log("category " + JSON.stringify(response));
       document.getElementById("createFilters").reset();
       $('.modal').modal('hide');
       $scope.getFilters();
     }).error(function (err) {
       console.log("failure");
     })
   }

   $scope.editFilter = function (id) {

     alert(id.id)
     $http({
       "method": "PUT",
       "url": URL + 'api/Filters/'+id,
       "headers": {"Content-Type": "application/json", "Accept": "application/json"},
       "data":$scope.editFiltersave
     }).success(function (response) {



       $scope.getFilters();
       $('.modal').modal('hide');
       Notification.success({message: 'Filter Successfully Upadted', delay: 1000});
     }).error(function (err) {
       console.log("failure");
     })
   };




 });


app.controller('productController', function ($http, $scope, $window, $location, $rootScope, Excel, $timeout, Notification) {
  console.log("productController");

  $scope.viewProduct = function () {
    $http({
      "method": "GET",
      "url": URL + 'api/Products',
      "headers": {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
      console.log("MealType " + JSON.stringify(response));
      $scope.MealType = response;
    }).error(function (err) {
      console.log("failure");
    })


  };

  $scope.createProduct = function () {
    if ($scope.MealTypeData.productName) {
        if ($scope.MealTypeData.status) {
          $http({
            method: 'POST',
            url: URL + 'api/Products',
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            "data": $scope.MealTypeData
          }).success(function (response) {
            $scope.viewProduct();
            document.getElementById("addmealTypeFormId").reset();
            $('#addmealTypePopId').modal('hide');
            Notification.success({message: 'Product Successfully added', delay: 1000});
          }).error(function (error) {
            Notification.error({message: error.error.message, delay: 2000});

          })
        } else {
          Notification.error({message: 'Please Select Status', delay: 1000});
        }

    } else {
      Notification.error({message: 'Please Enter MealType Desc', delay: 1000});
    }
  };

  //$scope.todayTop10select();
  $scope.editmealTypesavedata={};
  $scope.editmealType=function(obj){
    $scope.editmealTypesavedata.productName=obj.productName;
    $scope.editmealTypesavedata.status=obj.status;
    $scope.editmealTypesavedata.id=obj.id;
    $('#mealTypeEditpopId').modal('show');
  };

  $scope.editProduct = function (id) {
    $http({
      "method": "PUT",
      "url": URL + 'api/Products',
      "headers": {"Content-Type": "application/json", "Accept": "application/json"},
      "data":$scope.editmealTypesavedata
    }).success(function (response) {
      $scope.viewProduct();
      $('#mealTypeEditpopId').modal('hide');
      Notification.success({message: 'Product Successfully Upadted', delay: 1000});
      console.log("837mealtypeedit " + JSON.stringify(response));
    }).error(function (err) {
      console.log("921mealtypeedit " + JSON.stringify(err))
    })


  };
  $scope.exportToExcel = function (tableId) { // ex: '#my-table'
    var exportHref = Excel.tableToExcel(tableId, 'sheet name');
    $timeout(function () {
      location.href = exportHref;
    }, 100); // trigger download
  }
});




/*

app.controller('onlineShoppingController', function ($http, $window) {

  $http({
    "method":"GET"
  })

})
*/










app.factory('Excel', function ($window) {
  var uri = 'data:application/vnd.ms-excel;base64,',
    template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
    base64 = function (s) {
      return $window.btoa(unescape(encodeURIComponent(s)));
    },
    format = function (s, c) {
      return s.replace(/{(\w+)}/g, function (m, p) {
        return c[p];
      })
    };
  return {
    tableToExcel: function (tableId, worksheetName) {
      var table = $(tableId),
        ctx = {worksheet: worksheetName, table: table.html()},
        href = uri + base64(format(template, ctx));
      return href;
    }
  };
});

app.directive('validNumber', function () {
  return {
    require: '?ngModel',
    link: function (scope, element, attrs, ngModelCtrl) {
      if (!ngModelCtrl) {
        return;
      }
      ngModelCtrl.$parsers.push(function (val) {
        if (angular.isUndefined(val)) {
          var val = '';
        }
        var clean = val.replace(/[^0-9\.]/g, '');
        var negativeCheck = clean.split('-');
        var decimalCheck = clean.split('.');
        if (!angular.isUndefined(negativeCheck[1])) {
          negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
          clean = negativeCheck[0] + '-' + negativeCheck[1];
          if (negativeCheck[0].length > 0) {
            clean = negativeCheck[0];
          }
        }
        if (!angular.isUndefined(decimalCheck[1])) {
          decimalCheck[1] = decimalCheck[1].slice(0, 2);
          clean = decimalCheck[0] + '.' + decimalCheck[1];
        }
        if (val !== clean) {
          ngModelCtrl.$setViewValue(clean);
          ngModelCtrl.$render();
        }
        return clean;
      });
      element.bind('keypress', function (event) {
        if (event.keyCode === 32) {
          event.preventDefault();
        }
      });
    }
  };
});

app.filter('capitalize', function () {
  return function (input) {
    return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
  }
});

app.directive('capitalizes', function () {
  return {
    require: 'ngModel',
    link: function (scope, element, attrs, modelCtrl) {
      var capitalizes = function (inputValue) {
        if (inputValue == undefined) inputValue = '';
        var capitalized = inputValue.toUpperCase();
        if (capitalized !== inputValue) {
          modelCtrl.$setViewValue(capitalized);
          modelCtrl.$render();
        }
        return capitalized;
      }
      modelCtrl.$parsers.push(capitalizes);
      capitalizes(scope[attrs.ngModel]); // capitalize initial value
    }
  };
});

app.filter('orderObjectBy', function () {
  return function (items, field, reverse) {
    var filtered = [];
    angular.forEach(items, function (item) {
      filtered.push(item);
    });
    filtered.sort(function (a, b) {
      return (a[field] > b[field] ? 1 : -1);
    });
    if (reverse) filtered.reverse();
    return filtered;
  };

});

app.directive('fileModel', ['$parse', function ($parse) {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      var model = $parse(attrs.fileModel);
      var modelSetter = model.assign;

      element.bind('change', function () {
        scope.$apply(function () {
          modelSetter(scope, element[0].files[0]);
        });
      });
    }
  };
}]);

app.factory('fileUpload', ['$http', function ($http) {
  this.uploadFileToUrl = function (file, uploadUrl, cb) {
    var fd = new FormData();
    fd.append('file', file);
    $http.post(uploadUrl, fd, {
      transformRequest: angular.identity,
      headers: {'Content-Type': undefined}
    }).success(function (reponse) {
      cb(reponse);
      filedetails = reponse;
      // console.log("file upload reponse" + JSON.stringify(reponse));
    }).error(function (error) {
      cb(error);
      //console.log("reponse" + error);
    });
  };
  return this;
}]);







