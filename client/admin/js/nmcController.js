app.controller('nmcController', function ($route, $http, $scope, $window, $location, $rootScope, $compile, Excel, $timeout, Notification, uiCalendarConfig) {
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    $scope.loadingImage = true;
    //render NEW CALENDAR CODE
    var renderCalendar = function (Menus) {
        var md = '', mm = '', my = '';
        //alert('render calendar');
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            editable: true,
            year: y,
            month: m, // August
            events: Menus,
            eventClick: function (event, date, js) {
                md = new Date(event.start).getDate();
                mm = new Date(event.start).getMonth();
                my = new Date(event.start).getFullYear();

                if (event.title == 'Configure') {
                    $scope.cDate = ((mm < 10 ? "0" : "") + (mm + 1)) + '/' + ((md < 10 ? "0" : "") + (md)) + '/' + my;
                    $scope.$apply();
                    $('#configureModal').modal('show');
                    $scope.createMenu();
                }
                if (event.menuId != undefined && event.menuId != '') {
                    //$scope.eDate=  (mm+1)+'/'+md+'/'+my;
                    $scope.eDate = ((mm < 10 ? "0" : "") + (mm + 1)) + '/' + ((md < 10 ? "0" : "") + (md)) + '/' + my;
                    console.log('api/Menus?filter={"where":{"id":"' + event.menuId + '"}}');
                    $http({
                        method: 'GET',
                        url: URL + 'api/Menus?filter={"where":{"date":"' + event.date + '"}}',
                        headers: {"Content-Type": "application/json", "Accept": "application/json"}
                    }).success(function (response) {
                        $scope.items = response;
                    }).error(function (error) {

                    });
                    $('#editMenuModal').modal('show');
                    $scope.editMenu($scope.eDate);
                }
            },
            eventRender: function (event, element, view) {
                if (event.start.getMonth() !== view.start.getMonth()) {
                    return false;
                }
            }
        });
        $scope.loadingImage = false;
    }
    //get two months from the date of current month
    var getDaysInMonth = function (month, year) {
        return new Date(year, month, 0).getDate();
    }
    //get days for the next two months
    $scope.getDates = function (monthSelected, day, year) {
        var dates = [];
        for (var r = 1; r < 3; r++) {   //for the next two months
            for (var i = day; i < getDaysInMonth((monthSelected + r), year); i++) {
                $scope.day = i + 1;
                if (year == 12)//for the month of december
                {
                    year++;
                }
                var d = new Date(year, (monthSelected), $scope.day).getDay();//no need to pass r in new Date() method
                //restricting for the weekends
                if (d != 0 && d != 6) {
                    dates.push(new Date(year, (monthSelected), $scope.day));
                }
            }
            monthSelected++;
            day = 0;
        }
        return dates;
    }

    $scope.getDatesBetweenDates = function (startDate, endDate, i) {
        //console.log('getDatesBtwDates...start---'+startDate+'---end date--'+endDate);
        var dates = [],
            start = new Date(startDate),
            end = new Date(endDate),
            year = start.getFullYear(),
            month = start.getMonth()
        var day = start.getDate(), tempDates = [];
        if (i == 0) {
            dates = [start]
        } else {
            day = ++day;
            if (new Date(year, month, day).getDay() > 1) {
                dates.push(new Date(year, month, day));
            }
        }
        while (dates[dates.length - 1] < end) {
            day = ++day;
            //restricting for the weeekends
            if (new Date(year, month, day).getDay() > 0 && new Date(year, month, day).getDay() != 6) {
                dates.push(new Date(year, month, day));
            }
        }
        //console.log('dates---------'+JSON.stringify(dates));
        return dates;
    };

    $scope.getDatesBetweenExDates = function (startDate, endDate) {
        //console.log('getDatesBtwDate xxxxxx...start---'+new Date(startDate)+'---end date--'+new Date(endDate));
        var dates = [],
            start = new Date(startDate),
            end = new Date(endDate),
            year = start.getFullYear(),
            month = start.getMonth();
        var day = start.getDate(), s = start, e = end;

        Date.prototype.addDays = function (days) {
            var dat = new Date(this.valueOf())
            dat.setDate(dat.getDate() + days);
            return dat;
        }
        while (start <= end) {
            //console.log(s+'--'+start+'--'+e);
            if (start != s && start != e) {
                dates.push(start);
            }
            start = start.addDays(1);
        }
        dates.splice(dates.length - 1, 1);
        //console.log('dates---------'+JSON.stringify(dates));
        return dates;
    };

    $scope.getDateDifference = function (s, e) {
        //console.log('getDateDiff..'+s+'---'+e);
        var start = new Date(s);
        var end = new Date(e);
        var diff = new Date(end - start);
        var days = diff / 1000 / 60 / 60 / 24;
        return Math.round(Math.abs(days));
    }


    //get all items
    $scope.Is = [];
    $http({
        method: 'GET',
        url: URL + 'api/Categories',
        headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
        $scope.Is = response.data;
        console.log($scope.Is.length + '..');
    }).error(function (error) {

    })

    //$scope.days=$scope.getDates($scope.currentMonth);
    //get the dates from the current date to the end of the month iterate the json dates compare with the dates in menuItems dates if der is an object
    //then change the color of the date
    $scope.menus = [];
    $scope.events = [];
    //for each menu that and next one dates are taken n found dates between those two dates
    $http({
        method: 'GET',
        url: URL + 'api/Menus',
        headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
        $scope.menus = response.data;
        //sort all the dates
        $scope.menus.sort(function (d1, d2) {
            var dateA = new Date(d1.date), dateB = new Date(d2.date);
            return dateA - dateB;
        });
        $scope.days = [];
        var event = {};
        //construct the events for dates having menu-----1
        var d = '',done='';
        angular.forEach($scope.menus, function (v, i) {
            console.log('menu % configured..' + $scope.menus[i].Items.length + '--total items.....' + $scope.Is.length);
            $scope.$apply
            done= ($scope.menus[i].Items.length / $scope.Is.length) * 100;
            d = new Date(v.date);
            event = {
                title: 'View' + '(' + done + '% done)',
                start: new Date(d.getFullYear(), d.getMonth(), d.getDate()),
                menuId: v.id
            };
            $scope.events.push(event);
        });
        $scope.bdates = [];
        angular.forEach($scope.menus, function (menu, menuIndex) {
            $scope.days.push(menu.date);
            if ($scope.menus.length == $scope.days.length) {
                for (var x = 0; x < $scope.days.length; x++) {
                    if ($scope.getDateDifference($scope.days[x], $scope.days[x + 1]) > 1) {
                        $scope.bdates = $scope.getDatesBetweenExDates($scope.days[x], $scope.days[x + 1]);
                        angular.forEach($scope.bdates, function (mdate, dateindex) {
                            if (mdate.getDay() != 0 && mdate.getDay() != 6) {
                                event = {
                                    title: 'Configure',
                                    start: new Date(mdate.getFullYear(), mdate.getMonth(), mdate.getDate())
                                };
                                $scope.events.push(event);
                            }
                        });
                    }
                }
            }
        });

        renderCalendar($scope.events);
    });//http end






    //============================================To configure menu for new date
    $scope.createMenu = function () {
        var menuDateStatus;
        $scope.checkMenuDate = function (date) {
            console.log(date);
            $http({
                method: 'GET',
                url: URL + 'api/Menus?filter={"where":{"date":"' + date + '"}}',
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                if (response.data.length) {
                    delete $scope.cDate;
                    Notification.error('For this Date Menu is already configured Please Select Different Date');
                }
            }).error(function (error) {
                Notification.error("Oops.... try again");
            })

        };
        $scope.menu = [];
        $scope.itemIds = [];
        $scope.items = [];
        $scope.storedItems = [];
        $scope.finalitemCategoryNames = [];
        /*get category*/
        $scope.getCategroy = function () {
            $http({
                method: 'GET',
                url: URL + 'api/Categories?filter={"where":{"status":"available"}}',
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                $scope.categoryForDropdown = response.data;
            }).error(function (error) {

            })

        };
        $scope.getCategroy();
        /*end*/

        //editItemsArrayForCreate -- for the items selected in nested modal
        //ItemsForDropdownInPopUpForCreate -- for the items to be selected in nested modal



        //edit menu->edit item
        $scope.removeItemForCreate = function (itemAraay, index) {
            $scope.editcateArrayObjForCreate = $scope.menu[index];

            /*pop items dropdown*/
            $http({
                method: 'GET',
                url: URL + 'api/Items?filter={"where":{"categoryId":"' + $scope.editcateArrayObjForCreate.categoryId + '"}}',
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                if (response.data.length != 0) {
                    $scope.ItemsForDropdownInPopUpForCreate = response.data;
                    for (i = 0; i < $scope.ItemsForDropdownInPopUpForCreate.length; i++) {
                        for (j = 0; j < $scope.editcateArrayObjForCreate.itemIds.length; j++) {
                            if ($scope.ItemsForDropdownInPopUpForCreate[i].id == $scope.editcateArrayObjForCreate.itemIds[j]) {
                                $scope.ItemsForDropdownInPopUpForCreate.splice(i, 1);
                            }
                        }
                    }
                } else {
                    Notification.error('No items Avilable for selected Category');
                }

            }).error(function (error) {
                Notification.error('Oopss......1172');
            });
            $scope.editItemsArrayForCreate = [];
            $scope.itemmArryForCreate = itemAraay;
            $scope.indexForCreate = index;
            for (var i = 0; i < itemAraay.item.length; i++) {
                $scope.editItemsArrayForCreate.push(itemAraay.item[i])
            }
        };
        /*end*/

        $scope.spliceItemsForCreate = function (index) {
            if($scope.editItemsArrayForCreate.length>0) {
                $scope.editItemsArrayForCreate.splice(index, 1);
                $scope.editcateArrayObjForCreate.itemIds.splice(index, 1);
            }else{
                $('#removeItem').modal('hide');

            }
        };

        $scope.removeCategoryInCreate = function (index) {

            $scope.removedcategorytodropdown={};
            for(var x=0;x<$scope.Is.length;x++){
                if($scope.Is[x].name==$scope.finalitemCategoryNames[0].categry)
                    $scope.removedcategorytodropdown=$scope.Is[x];
            }
            //alert(JSON.stringify($scope.removedcategorytodropdown));
            $scope.finalitemCategoryNames.splice(index,1);
            $scope.menu.splice(index,1);

            $scope.categoryForDropdown.push($scope.removedcategorytodropdown);
            //console.log("menu length: " + JSON.stringify($scope.finalitemCategoryNames[index]));
            //console.log('$scope.categoryForDropdown in create--'+JSON.stringify($scope.categoryForDropdown));
        };


        /*get items start */
        $scope.getItems = function (categoryId) {
            $scope.selected = [];
            $scope.selectedCateId = categoryId;
            if ($scope.storedItems.length > 0) {
                for (var i = 0; i < $scope.storedItems.length; i++) {
                    $scope.exist = function (item) {
                        return $scope.selected.indexOf(item) > -1;
                    }
                }
            }
            $http({
                method: 'GET',
                url: URL + 'api/Items?filter={"where":{"categoryId":"' + categoryId + '"}}',
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                $scope.ItemsForDropdown = response.data;
            }).error(function (error) {
            })
        };

        $scope.selected1 = [];
        $scope.exist1 = function (mealType) {
            return $scope.selected1.indexOf(mealType) > -1;
        }

        $scope.toggleSelection1 = function (mealType) {
            var ids = $scope.selected1.indexOf(mealType);
            if (ids > -1) {
                $scope.selected1.splice(ids, 1)
            } else {
                $scope.selected1.push(mealType);
            }
        }

        $scope.selected = [];
        $scope.exist = function (item) {
            return $scope.selected.indexOf(item) > -1;
        };

        $scope.toggleSelection = function (item) {
            var ids = $scope.selected.indexOf(item);
            if (ids > -1) {
                $scope.selected.splice(ids, 1)
            } else {
                $scope.selected.push(item);
            }
        };

        //editing the configured items in create
        $scope.selected1ForCreate = [];
        $scope.exist1ForCreate = function (mealType) {
            return $scope.selected1ForCreate.indexOf(mealType) > -1;
        }
        $scope.toggleSelection1ForCreate = function (mealType) {
            var ids = $scope.selected1ForCreate.indexOf(mealType);
            if (ids > -1) {
                $scope.selected1ForCreate.splice(ids, 1)
            } else {
                $scope.selected1ForCreate.push(mealType);
            }
        }

        $scope.saveDetailsForCreate = function () {
            if ($scope.selected1ForCreate.length != 0) {
                for (i = 0; i < $scope.selected1ForCreate.length; i++) {
                    $scope.editItemsArrayForCreate.push($scope.selected1ForCreate[i].name);
                    $scope.editcateArrayObjForCreate.itemIds.push($scope.selected1ForCreate[i].id);

                    for (j = 0; j < $scope.ItemsForDropdownInPopUpForCreate.length; j++) {
                        if ($scope.selected1ForCreate[i].id == $scope.ItemsForDropdownInPopUpForCreate[j].id) {
                            $scope.ItemsForDropdownInPopUpForCreate.splice(j, 1);
                        }

                    }

                }
            }

            var data = {"categry": $scope.itemmArryForCreate.categry, "item": $scope.editItemsArrayForCreate};
            var dataId = {
                "categoryId": $scope.editcateArrayObjForCreate.categoryId,
                "itemIds": $scope.editcateArrayObjForCreate.itemIds
            };
            $scope.finalitemCategoryNames[$scope.indexForCreate] = data;
            $scope.menu[$scope.indexForCreate] = dataId;
            $scope.selected1ForCreate = [];

            $('#removeItem').modal('hide');

        };

        $scope.ItemsToDisply = function () {
            $scope.itemCategoryNames = [];
            for (var i = 0; i < $scope.selected.length; i++) {
                $scope.itemCategoryNames.push($scope.selected[i].name);
            }
            $http({
                method: 'GET',
                url: URL + 'api/Categories/' + $scope.selectedCateId,
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                $scope.selected = [];
                $scope.categoryNames = response.data.name;
                $scope.finalitemCategoryNames.push({
                    "categry": $scope.categoryNames,
                    "item": $scope.itemCategoryNames
                });
                //console.log("finalitemCategoryNames" + JSON.stringify($scope.finalitemCategoryNames));
                //console.log("menu" + JSON.stringify($scope.menu));
            }).error(function (error) {

            });

        };

        $scope.addCategoryToMenu = function (cateId) {
            if (cateId) {
                if ($scope.selected.length != 0) {
                    for (var i = 0; $scope.selected.length > i; i++) {
                        $scope.itemIds.push($scope.selected[i].id);
                    }
                    $scope.menu.push({"categoryId": cateId, "itemIds": $scope.itemIds});
                    console.log("lenght: " + $scope.categoryForDropdown.length);
                    for (j = 0; j < $scope.categoryForDropdown.length; j++) {
                        if ($scope.categoryForDropdown[j].id == cateId) {

                            $scope.categoryForDropdown.splice(j, 1);
                            // console.log("$scope.categoryForDropdown:" + JSON.stringify($scope.categoryForDropdown));
                            break;

                        }

                    }

                    // console.log("cateId:" + cateId);


                    $scope.ItemsToDisply();
                    $scope.itemIds = [];
                    $scope.ItemsForDropdown = {};
                    document.getElementById('selectCategoryId').value = '';
                    //$scope.selected = [];
                    //console.log("arry" + JSON.stringify($scope.menu));

                } else {

                    Notification.error('Please Select At least one Item');
                }

            } else {
                Notification.error('Please Select At least one Category');
            }


        };
        $scope.menuConfigure = function () {
            if ($scope.cDate) {
                if ($scope.menu != '') {
                    if ($scope.status) {
                        // console.log("menu>>>" + JSON.stringify($scope.menu));
                        $http({
                            method: 'POST',
                            url: URL + 'api/Menus',
                            headers: {"Content-Type": "application/json", "Accept": "application/json"},
                            "data": {
                                "date": $scope.cDate,
                                "status": $scope.status,
                                "Items": $scope.menu
                            }
                        }).success(function (response) {
                            $scope.cDate = '';
                            $scope.menu = [];
                            $scope.status = '';
                            $scope.finalitemCategoryNames = [];
                            $scope.getCategroy();
                            Notification.success({
                                message: 'Menu successfully configured for' + response.data.date,
                                delay: 1000
                            });
                            $('#configureModal').modal('hide');
                            $window.location.reload();
                            //alert("after Menus success " + JSON.stringify(response));
                        }).error(function (error) {
                            Notification.error({message: 'Oops.. try again', delay: 1000});
                            // alert("after Menus error " + JSON.stringify(error));
                        })

                    }
                    else {
                        Notification.error({message: 'Please Select Item Status', delay: 1000});

                    }
                } else {
                    Notification.error({message: 'Please Select Items for Menu', delay: 1000});

                }
            } else {
                Notification.error({message: 'Please Enter Date for Menu', delay: 1000});
            }
        }
    }






    //============================================To edit the existing menu
    $scope.editMenu = function (date) {
        $scope.loadingImage = true;
        //alert(date);
        $http({
            method: 'GET',
            url: URL + 'api/Menus?filter={"where":{"date":"' + date + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.items = response;
            // items=response;
            //console.log("$scope.items"+JSON.stringify($scope.items));

            $scope.menu = [];
            $scope.itemIds = [];

            $scope.storedItems = [];
            $scope.categoryItems = [];
            $scope.finalitemCategoryNamesEdit = [];
            /*edit menu start*/
            $scope.menuId = response.data[0].id;
            $scope.menuItems = $scope.items.data;
            $scope.finalitemCategoryNamesEdit=$scope.items.data;
            $scope.menuDate = $scope.items.data.date;
            //$scope.existingMenuItems=$scope.menuItems[0].Items;
            //console.log('finalitemCategoryNamesEdit....'+JSON.stringify($scope.finalitemCategoryNamesEdit));

            $http({
                method: 'GET',
                url: URL + 'api/Menus/' + $scope.menuId,
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                $scope.menu = response.data.Items;
                //console.log("$scope.menu: " + JSON.stringify(response.data));
                //filter the categories in drop down by removing the existing categories configured
                $http({
                    method: 'GET',
                    url: URL + 'api/Categories?filter={"where":{"status":"available"}}',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    $scope.categoryForDropdown = response.data;
                    console.log("562->>$scope.categoryForDropdown" + JSON.stringify($scope.categoryForDropdown));
                    for (i = 0; i < $scope.menu.length; i++) {
                        for (j = 0; j < $scope.categoryForDropdown.length; j++) {
                            if ($scope.categoryForDropdown[j].id == $scope.menu[i].categoryId) {
                                $scope.categoryForDropdown.splice(j, 1);
                            }
                        }
                    }
                }).error(function (error) {

                });

            }).error(function (error) {
                //console.log("error" + JSON.stringify(error));
            });
            /*edit menu end*/


            $scope.removeItem = function (itemAraay, index) {
                $scope.editcateArrayObj = $scope.menu[index];
                console.log('remove item: pop up data--api/Items?filter={"where":{"categoryId":"' + $scope.editcateArrayObj.categoryId + '"}}');
                $http({
                    method: 'GET',
                    url: URL + 'api/Items?filter={"where":{"categoryId":"' + $scope.editcateArrayObj.categoryId + '"}}',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    $scope.ItemsForDropdownInPopUp = response.data;
                    for (x = 0; x < $scope.editcateArrayObj.itemIds.length; x++) {
                        for (i = 0; i < $scope.ItemsForDropdownInPopUp.length; i++) {
                            //console.log($scope.ItemsForDropdownInPopUp[i].id+'--  i--'+i+' -- x  --'+ x +'----'+$scope.editcateArrayObj.itemIds[x]);
                            //console.log($scope.ItemsForDropdownInPopUp[i].id==$scope.editcateArrayObj.itemIds[x]);
                            if($scope.ItemsForDropdownInPopUp[i].id==$scope.editcateArrayObj.itemIds[x]){
                                //console.log(JSON.stringify($scope.ItemsForDropdownInPopUp));
                                $scope.ItemsForDropdownInPopUp.splice(i,1);
                            }
                        }
                    }
                }).error(function (error) {

                });

                $scope.editItemsArray = [];
                $scope.itemmArry = itemAraay;
                $scope.index = index;
                for (var i = 0; i < itemAraay.categoryItems.length; i++) {
                    $scope.editItemsArray.push(itemAraay.categoryItems[i])
                }
            };
            /*end*/
            $scope.spliceItems = function (index) {
                if ($scope.editItemsArray.length > 1) {
                    $scope.editItemsArray.splice(index, 1);
                    $scope.editcateArrayObj.itemIds.splice(index, 1);
                    //console.log('$scope.editItemsArray....' + JSON.stringify($scope.editItemsArray));
                    /*if($scope.editItemsArray.length==0){
                     removeCategory();
                     }*/
                } else {
                    Notification.error('Please Remove Category below.. ');
                }

            };

            $scope.removeCategoryNmc = function (index) {
                // alert(index);
                $scope.removedcategorytodropdown={};
                $scope.removedcategorytodropdown.name=$scope.menuItems[0].Items[index].categoryName;
                $scope.removedcategorytodropdown.id=$scope.menuItems[0].Items[index].categoryId;
                $scope.removedcategorytodropdown.status="available";
                //console.log("627"+JSON.stringify($scope.menuItems[0].Items[index]));
                // console.log("629"+JSON.stringify($scope.menu[index]));
                $scope.menuItems[0].Items.splice(index, 1);
                $scope.menu.splice(index, 1);
                $scope.categoryForDropdown.push($scope.removedcategorytodropdown);
                // console.log("menu length: "+JSON.stringify($scope.menu[index]));
                //console.log("633: "+$scope.menu.length);
                //console.log("634: "+$scope.categoryForDropdown.length);

            }

            $scope.saveDetails = function () {
                if ($scope.selected1.length != 0) {
                    for (i = 0; i < $scope.selected1.length; i++) {
                        $scope.editItemsArray.push($scope.selected1[i]);
                        console.log($scope.selected1[i].name);
                        $scope.editcateArrayObj.itemIds.push($scope.selected1[i].id);
                        console.log($scope.selected1[i].id);
                        for (j = 0; j < $scope.ItemsForDropdownInPopUp.length; j++) {
                            if ($scope.selected1[i].id == $scope.ItemsForDropdownInPopUp[j].id) {
                                $scope.ItemsForDropdownInPopUp.splice(j, 1);
                            }

                        }

                    }
                }

                var data = {"categoryName": $scope.itemmArry.categoryName, "categoryItems": $scope.editItemsArray};
                var dataId = {
                    "categoryId": $scope.editcateArrayObj.categoryId,
                    "itemIds": $scope.editcateArrayObj.itemIds
                };
                $scope.menuItems[0].Items[$scope.index] = data;
                $scope.menu[$scope.index] = dataId;
                $scope.selected1=[];
                $('#removeMenuItem').modal('hide');
            };

            $scope.selected1 = [];

            $scope.exist1 = function (mealType) {
                return $scope.selected1.indexOf(mealType) > -1;
            };


            $scope.toggleSelection1 = function (mealType) {
                var ids = $scope.selected1.indexOf(mealType);
                if (ids > -1) {
                    $scope.selected1.splice(ids, 1)
                } else {
                    $scope.selected1.push(mealType);
                }
            };

            $scope.selected = [];

            $scope.exist = function (item) {
                return $scope.selected.indexOf(item) > -1;
            };

            $scope.toggleSelection = function (item) {
                var ids = $scope.selected.indexOf(item);
                if (ids > -1) {
                    $scope.selected.splice(ids, 1)
                } else {
                    $scope.selected.push(item);
                }
            };

            $scope.getItems = function (categoryId) {
                $scope.selected = [];
                $scope.selectedCateId = categoryId;
                if ($scope.storedItems.length > 0) {
                    for (var i = 0; i < $scope.storedItems.length; i++) {
                        $scope.exist = function (item) {
                            return $scope.selected.indexOf(item) > -1;
                        }
                    }
                }
                $http({
                    method: 'GET',
                    url: URL + 'api/Items?filter={"where":{"categoryId":"' + categoryId + '"}}',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    $scope.ItemsForDropdown = response.data;
                }).error(function (error) {
                })
            };

            $scope.ItemsToDisply = function (cateId) {
                console.log("cateId" + cateId);
                $scope.itemCategoryNames = [];
                for (var i = 0; i < $scope.selected.length; i++) {
                    $scope.itemCategoryNames.push($scope.selected[i].name);
                }
                $http({
                    method: 'GET',
                    url: URL + 'api/Categories/' + $scope.selectedCateId,
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    $scope.selected = [];
                    $scope.categoryNames = response.data.name;
                    //$scope.existingMenu.push({
                      $scope.menuItems[0].Items.push({
                        "categoryId": cateId,
                        "itemIds": $scope.itemIds,
                        "categoryName": $scope.categoryNames,
                        "categoryItems": $scope.categoryItems
                    });
                    $scope.itemIds = [];
                    $scope.categoryItems = [];
                }).error(function (error) {

                });
            };

            $scope.addCategoryToMenuInEdit = function (cateId) {
                //console.log("cateId"+cateId.length);
                if(cateId!=undefined) {
                    if ($scope.selected.length != 0) {
                        for (var i = 0; $scope.selected.length > i; i++) {
                            console.log("selected" + JSON.stringify($scope.selected[i].id));
                            $scope.itemIds.push($scope.selected[i].id);
                            $scope.categoryItems.push($scope.selected[i]);
                        }
                        $scope.menu.push({"categoryId": cateId, "itemIds": $scope.itemIds});
                        console.log("lenght: " + $scope.categoryForDropdown.length);
                        for (j = 0; j < $scope.categoryForDropdown.length; j++) {
                            if ($scope.categoryForDropdown[j].id == cateId) {

                                $scope.categoryForDropdown.splice(j, 1);
                                // console.log("$scope.categoryForDropdown:" + JSON.stringify($scope.categoryForDropdown));
                                break;
                            }
                        }
                        $scope.ItemsToDisply(cateId);
                        // $scope.itemIds = [];
                        $scope.ItemsForDropdown = {};
                        document.getElementById('selectCategoryId').value = '';
                    }else{
                        Notification.error('Please Select At least one Item');
                    }
                }else {
                    Notification.error('Please Select At least one Category');
                }
            };

            $scope.editMenuSave = function (id, date, status) {
                console.log("$scope.menu" + JSON.stringify($scope.menu));
                console.log("id  " + id);
                console.log("date " + date);
                console.log("status " + status);
                $http({
                    "method": "PUT",
                    "url": URL + 'api/Menus/' + id,
                    "headers": {'Content-Type': 'application/json', 'Accept': 'application/json'},
                    "data": {
                        "date": date,
                        "status": status,
                        "Items": $scope.menu
                    }
                }).success(function (response) {
                    // console.log("menu update Success" + JSON.stringify(response));
                    Notification.success('Menu updated succesfully');
                    $scope.loadingImage = false;
                    $window.location.reload();
                }).error(function (response) {
                    $scope.loadingImage = false;
                    console.log("Error" + JSON.stringify(response));
                })
            }

        }).error(function (error) {
        });

    }






    $scope.exportToExcel = function (tableId) { // ex: '#my-table'
        var oTable = document.getElementById('table1');
        //gets rows of table
        var rowLength = oTable.rows.length;
        if (rowLength > 0) {
            //gets cells of current row
            var oCells = oTable.rows.item(1).cells;

            //gets amount of cells of current row
            var cellLength = oCells.length;
            if (cellLength <= 8) {
                var exportHref = Excel.tableToExcel(tableId, 'sheet name');
                $timeout(function () {
                    location.href = exportHref;
                }, 100); // trigger download
            } else {
                Notification.error({message: 'No Data Found ', delay: 1000});
            }
        }
    }

    //============================================End
    /*To close the modal*/
    $scope.cancel = function () {
        $('#configureModal').modal('toggle');
    };

});